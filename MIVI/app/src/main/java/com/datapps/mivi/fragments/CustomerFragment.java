package com.datapps.mivi.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.datapps.mivi.R;
import com.datapps.mivi.model.Collection;
import com.google.gson.Gson;

import java.io.InputStream;
import java.util.Scanner;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerFragment extends DialogFragment {


    public CustomerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer, container, false);

        Collection data = readJsonFile();

        TextView tvPaymentType = view.findViewById(R.id.tv_payment_type);
        TextView tvFirstName = view.findViewById(R.id.tv_first_name);
        TextView tvDob = view.findViewById(R.id.tv_dob);
        TextView tvEmail = view.findViewById(R.id.tv_email);
        TextView tvContact = view.findViewById(R.id.tv_contact);
        TextView ok = view.findViewById(R.id.customer_ok);

        String type = data.getData().getAttribute().getPaymentType();
        String name = data.getData().getAttribute().getFirstName()+" "+data.getData().getAttribute().getLastName();
        String dob = data.getData().getAttribute().getDateOfBirth();
        String email = data.getData().getAttribute().getEmailAddress();
        String contact = data.getData().getAttribute().getContactNumber();

        tvPaymentType.setText(type);
        tvFirstName.setText(name);
        tvDob.setText(dob);
        tvEmail.setText(email);
        tvContact.setText(contact);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

    public Collection readJsonFile() {
        Resources res = getResources();
        InputStream in = res.openRawResource(R.raw.collection);

        Scanner scanner = new Scanner(in);
        StringBuilder stringBuilder = new StringBuilder();

        while (scanner.hasNextLine()){
            stringBuilder.append(scanner.nextLine());
        }

        try {
            Gson gson = new Gson();
            Collection m = gson.fromJson(String.valueOf(stringBuilder), Collection.class);
            return m;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
//        Toast.makeText(mContext, stringBuilder, Toast.LENGTH_SHORT).show();
    }

}
