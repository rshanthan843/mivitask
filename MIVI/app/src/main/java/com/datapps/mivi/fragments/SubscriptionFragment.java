package com.datapps.mivi.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.datapps.mivi.R;
import com.datapps.mivi.model.Collection;
import com.google.gson.Gson;

import java.io.InputStream;
import java.util.Scanner;

public class SubscriptionFragment extends DialogFragment {

    public SubscriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subscription, container, false);

        Collection data = readJsonFile();

        TextView individualBalance = view.findViewById(R.id.tv_balance);
        TextView expiryDate = view.findViewById(R.id.tv_expiry_date);
        TextView ok = view.findViewById(R.id.subscription_ok);

        String bal = String.valueOf(data.getIncluded().get(1).getAttributes().getIncludedDataBalance());
        String expDate = data.getIncluded().get(1).getAttributes().getExpiryDate();

        individualBalance.setText(bal);
        expiryDate.setText(expDate);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }


    public Collection readJsonFile() {
        Resources res = getResources();
        InputStream in = res.openRawResource(R.raw.collection);

        Scanner scanner = new Scanner(in);
        StringBuilder stringBuilder = new StringBuilder();

        while (scanner.hasNextLine()){
            stringBuilder.append(scanner.nextLine());
        }

        try {
            Gson gson = new Gson();
            Collection m = gson.fromJson(String.valueOf(stringBuilder), Collection.class);
            return m;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
//        Toast.makeText(mContext, stringBuilder, Toast.LENGTH_SHORT).show();
    }

}
