package com.datapps.mivi;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.datapps.mivi.R;
import com.datapps.mivi.model.Collection;
import com.google.gson.Gson;

import java.io.InputStream;
import java.util.Scanner;

public class MIVIActivity extends AppCompatActivity {

    public static final String TAG = "MIVIActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Hide title bar
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mivi);
    }

    public Collection readJsonFile() {
        Resources res = getResources();
        InputStream in = res.openRawResource(R.raw.collection);

        Scanner scanner = new Scanner(in);
        StringBuilder stringBuilder = new StringBuilder();

        while (scanner.hasNextLine()){
            stringBuilder.append(scanner.nextLine());
        }

        try {
            Gson gson = new Gson();
            Collection m = gson.fromJson(String.valueOf(stringBuilder), Collection.class);

            Log.d(TAG, "MSN : "+m.getIncluded().get(0).getAttributes().getMsn());
            return m;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
//        Toast.makeText(mContext, stringBuilder, Toast.LENGTH_SHORT).show();
    }
}
