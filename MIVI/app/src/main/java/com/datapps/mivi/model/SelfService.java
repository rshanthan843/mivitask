package com.datapps.mivi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelfService {
    @SerializedName("links")
    @Expose
    private SelfLinks links;

    public SelfLinks getLinks() {
        return links;
    }

    public void setLinks(SelfLinks links) {
        this.links = links;
    }
}
