package com.datapps.mivi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RelatedService {
    @SerializedName("links")
    @Expose
    private RelatedService links;

    public RelatedService getLinks() {
        return links;
    }

    public void setLinks(RelatedService links) {
        this.links = links;
    }
}
