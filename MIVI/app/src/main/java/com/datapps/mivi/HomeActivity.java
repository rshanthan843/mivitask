package com.datapps.mivi;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

import com.datapps.mivi.fragments.CustomerFragment;
import com.datapps.mivi.fragments.ProductFragment;
import com.datapps.mivi.fragments.SubscriptionFragment;
import com.datapps.mivi.model.Collection;

public class HomeActivity extends MIVIActivity {

    public static final String TAG = "HomeActivity";
    private Context mContext = this;
    private TextView tvMsnNo;
    private TextView tvMsnCredit;
    private TextView tvSubscriptionInfo;
    private TextView tvProductName;
    private TextView tvProductPrice;
    private TextView tvProductInfo;
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvCustomerInfo;
    private Collection data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide title bar
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);

        data = readJsonFile();

        tvMsnNo = findViewById(R.id.tv_msn_no);
        tvMsnCredit = findViewById(R.id.tv_msn_credit);
        tvSubscriptionInfo = findViewById(R.id.subscription_info);
        tvProductName = findViewById(R.id.tv_p_name);
        tvProductPrice = findViewById(R.id.tv_p_price);
        tvProductInfo = findViewById(R.id.tv_product_info);
        tvFirstName = findViewById(R.id.tv_f_name);
        tvLastName = findViewById(R.id.tv_l_name);
        tvCustomerInfo = findViewById(R.id.tv_customer_info);


        String fName = data.getData().getAttribute().getFirstName();
        String lName = data.getData().getAttribute().getLastName();
        tvFirstName.setText(fName);
        tvLastName.setText(lName);

        String msnNo = data.getIncluded().get(0).getAttributes().getMsn();
        Integer msnCredit = data.getIncluded().get(0).getAttributes().getCredit();
        tvMsnNo.setText(msnNo);
        tvMsnCredit.setText(String.valueOf(msnCredit));

        String pName = data.getIncluded().get(2).getAttributes().getName();
        Integer pPrice = data.getIncluded().get(2).getAttributes().getPrice();
        tvProductName.setText(pName);
        tvProductPrice.setText(String.valueOf(pPrice));

        tvSubscriptionInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                DialogFragment dialogFragment = new SubscriptionFragment();
                dialogFragment.show(fragmentTransaction, "dialog");
            }
        });

        tvProductInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                DialogFragment dialogFragment = new ProductFragment();
                dialogFragment.show(fragmentTransaction, "dialog");
            }
        });

        tvCustomerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                DialogFragment dialogFragment = new CustomerFragment();
                dialogFragment.show(fragmentTransaction, "dialog");
            }
        });

    }
}
