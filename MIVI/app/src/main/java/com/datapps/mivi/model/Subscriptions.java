package com.datapps.mivi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Subscriptions {
    @SerializedName("links")
    @Expose
    private RelatedLinks links;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public RelatedLinks getLinks() {
        return links;
    }

    public void setLinks(RelatedLinks links) {
        this.links = links;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
