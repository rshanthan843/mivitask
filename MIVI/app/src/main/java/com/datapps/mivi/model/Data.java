package com.datapps.mivi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("attributes")
    @Expose
    private Attribute attributes;
    @SerializedName("links")
    @Expose
    private SelfLinks links;
    @SerializedName("relationships")
    @Expose
    private Relationships relationships;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Attribute getAttribute() {
        return attributes;
    }

    public void setAttribute(Attribute attributes) {
        this.attributes = attributes;
    }

    public SelfLinks getLinks() {
        return links;
    }

    public void setLinks(SelfLinks links) {
        this.links = links;
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }
}
